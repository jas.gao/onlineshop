﻿using Microsoft.AspNetCore.Mvc;
using OnlineShop.Application.ProductsAdmin;
using OnlineShop.Database;
using GetProducts = OnlineShop.Application.ProductsAdmin.GetProducts;

namespace OnlineShop.UI.Controllers
{
    [Route("[controller]")]
    public class AdminController : Controller
    {
        private ApplicationDbContext _context;
        public AdminController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("products")]
        public IActionResult GetProducts() => Ok(new GetProducts(_context).Do());  
        
        [HttpGet("product/{id}")]
        public IActionResult GetProduct(int id) => Ok(new GetProduct(_context).Do(id));  
        
        [HttpPost("products")]
        public async Task<IActionResult>  CreateProduct([FromBody] CreateProducts.Request vm)
        {
            var create= new CreateProducts(_context);
            var response = await create.Add(vm);
            return Ok(response);
        }
        
        [HttpDelete("products/{id}")]
        public async Task<IActionResult> DeleteProduct(int id) => Ok(await new DeleteProduct(_context).Do(id));  
        
        [HttpPut("products")]
        public async Task<IActionResult> UpdateProduct([FromBody] UpdateProduct.Request vm) => Ok(await new UpdateProduct(_context).Do(vm));


        
    }
}
