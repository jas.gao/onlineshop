﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineShop.Application.Products;
using OnlineShop.Application.ProductsAdmin;
using OnlineShop.Database;
using GetProducts = OnlineShop.Application.Products.GetProducts;

namespace OnlineShop.UI.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly ApplicationDbContext _context;

       

        [BindProperty] public CreateProducts.Request Product { get; set; }

        public IndexModel(ILogger<IndexModel> logger, ApplicationDbContext context)

        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<GetProducts.ProductViewModel> Products { get; set; }
        public void OnGet()
        {
            Products = new GetProducts(_context).Do().ToList();

        }

       
    }
}