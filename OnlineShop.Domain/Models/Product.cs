﻿namespace OnlineShop.Domain.Models
{
    public class Product
    {
        private string name;

        public int Id { get; set; }
        public string Name { get => name; set => name = value; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public ICollection<Stock>? Stock { get; set; }
    }
}
