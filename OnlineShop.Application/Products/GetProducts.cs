﻿using OnlineShop.Database;

namespace OnlineShop.Application.Products
{
    public class GetProducts
    {
        private ApplicationDbContext _db;

        public GetProducts(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<ProductViewModel> Do() =>
            _db.Products.ToList().Select(p => new ProductViewModel
            {
                Description = p.Description,
                Name = p.Name, 
                Value = $"£ {p.Value:N2}"
            });

        public class ProductViewModel
        {
            public string Value { get; set; }
            public string Name { get; set; }

            public string Description { get; set; }
        }
    }
   
}
