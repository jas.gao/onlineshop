﻿using OnlineShop.Database;

namespace OnlineShop.Application.ProductsAdmin
{
    public class UpdateProduct
    {
        private ApplicationDbContext _context;

        public UpdateProduct(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Response> Do(Request vm)
        {
            var product = _context.Products.FirstOrDefault(p => p.Id == vm.Id);

            if (product != null)
            {
                product.Name = vm.Name;
                product.Description = vm.Description;
                product.Value = vm.Value;
            }

            await _context.SaveChangesAsync();
            return new Response
            {
                Value = product.Value,
                Id = product.Id,
                Description = product.Description,
                Name = product.Name
            };
        }

        public class Request
        {
            public decimal Value { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
            public string Description { get; set; }
        }

        public class Response 
        {
            public decimal Value { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
            public string Description { get; set; }
        }
    }
}
