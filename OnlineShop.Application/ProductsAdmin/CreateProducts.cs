﻿using OnlineShop.Database;
using OnlineShop.Domain.Models;

namespace OnlineShop.Application.ProductsAdmin;

public class CreateProducts
{
    private ApplicationDbContext _context;

    public CreateProducts(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Response> Add(Request vm)
    {
        var product = new Product
        {
            Description = vm.Description,
            Value = vm.Value,
            Name = vm.Name
        };
        _context.Products.Add(product);
        await _context.SaveChangesAsync();
        return new Response
        {
            Id = product.Id,
            Name = product.Name,
            Description = product.Description,
            Value = product.Value
        };
    }
    public class Request
    {
        private string name;

        public decimal Value { get; set; }
        public string Name { get => name; set => name = value; }
        public string Description { get; set; }
    }
    public class Response
    {
        private string name;
        public int Id { get; set; }
        public decimal Value { get; set; }
        public string Name { get => name; set => name = value; }
        public string Description { get; set; }
    }
}
