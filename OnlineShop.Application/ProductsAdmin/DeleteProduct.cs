﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.Database;

namespace OnlineShop.Application.ProductsAdmin
{
    public class DeleteProduct
    {
        private ApplicationDbContext _context;

        public DeleteProduct(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Do(int id)
        {
            var product = await _context.Products.FirstOrDefaultAsync(p=>p.Id==id);
            if (product != null)
            {
                _context.Remove(product);
                await _context.SaveChangesAsync();
            }

            return true;
        }

    }
}
