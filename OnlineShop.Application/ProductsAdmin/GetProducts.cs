﻿using OnlineShop.Database;

namespace OnlineShop.Application.ProductsAdmin
{
    public class GetProducts
    {
        private ApplicationDbContext _db;

        public GetProducts(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<ProductViewModel> Do() =>
            _db.Products.Select(p => new ProductViewModel
            {
                Name = p.Name, 
                Value = p.Value, 
                Id = p.Id
            });

        public class ProductViewModel
        {
            public decimal Value { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
        }
    }
   
}
