﻿using OnlineShop.Database;

namespace OnlineShop.Application.ProductsAdmin
{
    public class GetProduct
    {
        private ApplicationDbContext _db;

        public GetProduct(ApplicationDbContext db)
        {
            _db = db;
        }

        public ProductViewModel? Do(int id) =>
            _db.Products.Where(x=>x.Id==id).Select(p => new ProductViewModel
            {
                Description = p.Description,
                Name = p.Name, 
                Value = p.Value, 
                Id = p.Id
            }).FirstOrDefault();

        public class ProductViewModel
        {
            public decimal Value { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
            public string Description { get; set; }
        }
    }
   
}
