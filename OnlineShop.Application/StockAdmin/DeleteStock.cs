﻿using OnlineShop.Database;
using OnlineShop.Domain.Models;

namespace OnlineShop.Application.StockAdmin;

public class DeleteStock
{
    private readonly ApplicationDbContext _ctx;

    public DeleteStock(ApplicationDbContext ctx)
    {
        _ctx = ctx;
    }

    public async Task<bool> Do(int id)
    {
        var stock = _ctx.Stocks.FirstOrDefault(p => p.Id == id);
        _ctx.Stocks.Remove(stock);
        await _ctx.SaveChangesAsync();
        return true;
    }
}