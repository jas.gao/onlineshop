﻿using OnlineShop.Database;
using OnlineShop.Domain.Models;

namespace OnlineShop.Application.StockAdmin;

public class UpdateStock
{
    private readonly ApplicationDbContext _ctx;

    public UpdateStock(ApplicationDbContext ctx)
    {
        _ctx = ctx;
    }

    public async Task<Response> Do(Request request)
    {
        var stocks = new List<Stock>();

        foreach (var stock in request.Stocks)
        {
            var update = new Stock
            {
                Description = stock.Description,
                Qty = stock.Qty,
                ProductId = stock.ProductId, 
                Id = stock.Id
            };
            stocks.Add(update);
        }

        _ctx.Stocks.UpdateRange(stocks);
        await _ctx.SaveChangesAsync();
        return new Response
        {
           Stocks = request.Stocks
        };
    }
    public class StockViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int ProductId { get; set; }
    }
    public class Request
    {
       public IEnumerable<StockViewModel> Stocks { get; set; }
    }
    public class Response
    {
        public IEnumerable<StockViewModel> Stocks { get; set; }
    }

}