﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.Database;
using OnlineShop.Domain.Models;

namespace OnlineShop.Application.StockAdmin;

public class GetStocks
{
    private readonly ApplicationDbContext _ctx;

    public GetStocks(ApplicationDbContext ctx)
    {
        _ctx = ctx;
    }

    public async Task<IEnumerable<StockViewModel>> Do(int productId)
    {
        var  stocks = await _ctx.Stocks.Where(p => p.ProductId == productId)
            .Select(p=> new StockViewModel
            {
                Description = p.Description, 
                Id = p.Id, 
                Qty = p.Qty, 
            }).ToListAsync();
        return stocks;
    }
    public class StockViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
    }
}